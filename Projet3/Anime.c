#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void draw_rec(SDL_Renderer* renderer,int rectWidth, int rectHeight,int rectx,int recty,int Red,int Green,int Blue)
{  
    SDL_Rect rect;
    rect.x = rectx;
    rect.y = recty;
    rect.w = rectWidth;
    rect.h = rectHeight;
    SDL_SetRenderDrawColor(renderer,Red,Green,Blue,200);
    SDL_RenderFillRect(renderer, &rect);

}
int genererNombreAleatoire() {
    // Initialisation de la graine pour la génération aléatoire
    

    // Génération d'un nombre aléatoire entre -50 et 50
    int nombreAleatoire = rand() % 101 - 50;

    return nombreAleatoire;
}

void background(SDL_Renderer* renderer)
{
    draw_rec(renderer,900,200,0,300,203,171,111);//sable
    draw_rec(renderer,900,150,0,150,65,105,225);//mer
    draw_rec(renderer,900,200,0,0,119,181,254);//ciel
}

SDL_Texture* creation_Texture(SDL_Renderer* renderer)
{
SDL_Texture *my_texture; 
  my_texture = IMG_LoadTexture(renderer,"./Bien.png");
  if (my_texture == NULL){
  IMG_Quit();
  exit(EXIT_FAILURE);
  }
  return my_texture;
}
void play_with_texture_4(SDL_Texture* my_texture,
               SDL_Window* window,
               SDL_Renderer* renderer) {
     SDL_Rect 
           source = {0},                    // Rectangle définissant la zone totale de la planche
           window_dimensions = {0},         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
           destination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
           state = {0};                     // Rectangle de la vignette en cours dans la planche 

     SDL_GetWindowSize(window,              // Récupération des dimensions de la fenêtre
               &window_dimensions.w,
               &window_dimensions.h);
     SDL_QueryTexture(my_texture,           // Récupération des dimensions de l'image
              NULL, NULL,
              &source.w, &source.h);

     /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */

     int nb_images = 14;                     // Il y a 14 vignette dans la ligne de l'image qui nous intéresse
     float zoom = 2;                        // zoom, car ces images sont un peu petites
     int offset_x = (source.w / nb_images),   // La largeur d'une vignette de l'image, marche car la planche est bien réglée
         offset_y = source.h / 6;           // La hauteur d'une vignette de l'image, marche car la planche est bien réglée
       

     state.x = 20;                          // La première vignette est en début de ligne
     state.y = 5* offset_y;                // On s'intéresse à la 2ème ligne,
     state.w = offset_x;                    // Largeur de la vignette
     state.h = offset_y;                    // Hauteur de la vignette

     destination.w = offset_x * zoom;       // Largeur du sprite à l'écran
     destination.h = offset_y * zoom;       // Hauteur du sprite à l'écran

     destination.y =   (window_dimensions.h - destination.h) /1.5;                     // La course se fait en tier d'écran (en vertical)

     int speed = 9;
     for (int x = 0; x < window_dimensions.w - destination.w; x += speed) {
       destination.x = x;                   // Position en x pour l'affichage du sprite
       state.x += offset_x;                 // On passe à la vignette suivante dans l'image
       state.x %= source.w;                 // La vignette qui suit celle de fin de ligne est
                        // celle de début de ligne

       SDL_RenderClear(renderer);           // Effacer l'image précédente avant de dessiner la nouvelle
       background(renderer);
       SDL_RenderCopy(renderer, my_texture, // Préparation de l'affichage
              &state,
              &destination);  
       SDL_RenderPresent(renderer);         // Affichage
       SDL_Delay(80);                       // Pause en ms
     }
     SDL_RenderClear(renderer);             // Effacer la fenêtre avant de rendre la main
       }



int main()

{
    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("fenetre", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 900, 500, SDL_WINDOW_SHOWN);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    SDL_Texture* my_texture =creation_Texture(renderer);
    if (renderer == 0) {
    fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());}
    SDL_Event event;
    int running = 1;
    int i;
while (running) {
        while (SDL_PollEvent(&event)) {
            switch (event.type){
            
            case SDL_QUIT:
                running = 0;
                break;
            case SDL_MOUSEMOTION:
            {
                SDL_RenderPresent(renderer);
                background(renderer);
                
                int mouseX,mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                printf("Coordonnées du curseur en : X = %d, Y = %d\n", mouseX, mouseY);
                for(i=0;i<8;i++)
                {   int a=genererNombreAleatoire();
                    printf ("%d\n,",a);
                    draw_rec(renderer,10,5,100+a+i*60,175+a,255,255,255);//nuages
                    draw_rec(renderer,10,5,220+a+i*60,250+a,255,255,255);//nuages
                    SDL_Delay(40);

                    
                }
                 break;
            }
            case SDL_KEYDOWN:
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_t:
                {
                SDL_RenderPresent(renderer);
                int mouseX,mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                printf("Coordonnées du curseur en moon walk : X = %d, Y = %d\n", mouseX, mouseY);
                play_with_texture_4(my_texture,window,renderer);
                
                    break;
                }
                case SDLK_v:{
                
                break;
                }
                }
            }

            
            

           

            default:
                break;
        }
    }
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_DestroyTexture(my_texture);
    SDL_Quit();
    return 0;
}


