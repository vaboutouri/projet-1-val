#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>



void draw_blue (SDL_Renderer* renderer,int rectWidth, int rectHeight){
    // Dimensions des rectangles
    

    // Rectangle bleu
    SDL_Rect rectBlue;
    rectBlue.x = 0;
    rectBlue.y = 0;
    rectBlue.w = rectWidth;
    rectBlue.h = rectHeight;
    SDL_SetRenderDrawColor(renderer, 0, 85, 164, 255); // Couleur bleue
    SDL_RenderFillRect(renderer, &rectBlue);
}

void draw_white (SDL_Renderer* renderer,int rectWidth, int rectHeight){
    // Rectangle blanc
    SDL_Rect rectWhite;
    rectWhite.x = rectWidth;
    rectWhite.y = 0;
    rectWhite.w = rectWidth;
    rectWhite.h = rectHeight;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Couleur blanche
    SDL_RenderFillRect(renderer, &rectWhite);}


void draw_red (SDL_Renderer* renderer,int rectWidth, int rectHeight){

    // Rectangle rouge
    SDL_Rect rectRed;
    rectRed.x = rectWidth * 2;
    rectRed.y = 0;
    rectRed.w = rectWidth;
    rectRed.h = rectHeight;

    SDL_SetRenderDrawColor(renderer, 239, 65, 53, 255); // Couleur rouge
    SDL_RenderFillRect(renderer, &rectRed);
    }


int nombreAleatoire() {
    srand(time(NULL)); // Initialisation de la graine pour la génération de nombres aléatoires

    int nombre = rand() % 256; // Génération d'un nombre aléatoire entre 0 et 255

    return nombre;
}



void draw_aléa(SDL_Renderer* renderer,int rectWidth, int rectHeight,int rectx)
{   int nbalea1=nombreAleatoire();
    int nbalea2=nombreAleatoire();
    int nbalea3=nombreAleatoire();
    SDL_Rect rect;
    rect.x = rectx;
    rect.y = 0;
    rect.w = rectWidth;
    rect.h = rectHeight;
    SDL_SetRenderDrawColor(renderer, 0, nbalea1,nbalea2, nbalea3); // Couleur aléa
    SDL_RenderFillRect(renderer, &rect);

}




int chanceSurTrois() {
    srand(time(NULL)); // Initialisation de la graine pour la génération de nombres aléatoires

    int randomValue = rand() % 3; // Génération d'un nombre aléatoire entre 0 et 2

    if (randomValue == 0) {
        return 1;
    } else {
        return 0;
    }
}




int main() {
    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("fenetre", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 900, 500, SDL_WINDOW_SHOWN);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == 0) {
    fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());}


 SDL_Event event;
    int running = 1;
    int i;
    int ib=0;
    int iw=0;
    int ir=0;

    while (running) {
        while (SDL_PollEvent(&event)) {
            switch (event.type){
            
            case SDL_QUIT:
                running = 0;
                break;
            case SDL_MOUSEMOTION:
            {
                SDL_RenderPresent(renderer);
                int mouseX,mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                printf("Coordonnées du curseur : X = %d, Y = %d\n", mouseX, mouseY);
                draw_blue(renderer,300,600);
                draw_white(renderer,300,600);
                draw_red(renderer,300,600);
                
                if (mouseX<300 && ib==0) //curseur sur carré bleu
                {ib=1;
                    for (i=0;i<60;i++)
                    {
                    draw_aléa(renderer,300+(i*10),600,0);
                    SDL_RenderPresent(renderer);
                    SDL_Delay(1);
                    printf("on est au bleu : X = %d, Y = %d\n", mouseX, mouseY);  
                    }
                }
               if (mouseX>300 && mouseX<600 && iw==0)//curseru sur carré blanc
                {iw=1;
                    for (i=0;i<60;i++)
                    {
                    draw_aléa(renderer,300-(i*5),600,300);
                    draw_aléa(renderer,300+(i*5),600,300);
                    SDL_RenderPresent(renderer);
                    SDL_Delay(1);  
                    }
                }
                if (mouseX>600 && ir==0)//curseur sur carré rouge
                {ir=1;
                    for (i=0;i<60;i++)
                    {
                    draw_aléa(renderer,300+(i*5),600,600);
                    draw_aléa(renderer,300-(i*10),600,600);
                    SDL_RenderPresent(renderer);
                    SDL_Delay(1);  
                    }
                }
                break;
            }
            case SDL_MOUSEBUTTONDOWN:{//si j'appuie clique gauche une chance sur 3 de changer la couleur
                ib=chanceSurTrois();
                iw=chanceSurTrois();
                ir=chanceSurTrois();}


            break;
            case SDLK_t:
            //translation d'un rectangel de droite à gauche
            break;

            default:
                break;
        }
    }
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
